# Nano: Design do Protocolo

O white paper ainda é um trabalho em andamento, segundo a própria Nano Foundation

## Abstract

- Arquitetura Block-Lattice

- Cada conta tem sua própria Blockchain

- Cada Bloco é uma transação (de envio OU recebimento, além da Epoch e Change)

- Uma Transferência precisa de duas transações (de envio E de recebimento)

- Open Representative Voting (ORV) como mecanismo de consenso

## Introdução

- Cada usuário atualiza sua própria Blockchain de forma assíncrona com relação às outras transações na rede

- Transações registram os saldos de contas, ao invés de registrar valores de transação
  
  - Ou seja: são transmitidos os saldos finais, ao invés de transmitirem o valor a ser transferido

- Permite encurtar a base de dados sem comprometer a segurança

- Consenso é mantido pelo ORV (Open Representative Voting, Voto Aberto de Representante)

- O representante escolhido pelo usuário vota em cada uma de suas transações

- Cada node "cimenta" (registra de forma localmente irreversível) cada transação, de forma independente dos demais, após verificar que um número suficiente de representantes votaram de forma a alcançar o *quorum* (quando a diferença entre dois blocos sucessivos é maior que 67% dos votos de representantes)

## Design da Ledger

- A Ledger da NANO é um conjunto global de contas, na qual cada conta contém sua própria Blockchain (cadeia de blocos/transações)

- É um componente chave do design, que troca o "acordo em tempo de execução" por um "acordo em tempo de design"

- Todos acordam via verificação de assinaturas que apenas o dono de uma conta pode modificar seu saldo e node representante

- Converte uma estrutura de dados compartilhada (cadeia de transações globais) em um conjunto de estruturas de dados não compartilhadas (cadeias de transações individuais)

- Cada node determina se vai ou não adicionar uma transação válida em sua ledger local
  
  - Isso elimina a necessidade de um mineirador (ou staker, em caso de PoS) de extender a blockchain com um bloco contendo transações válidas após solucionar um PoW através de uma seleção aleatória de transações

- A arquitetura Block-Lattice remove esse gargalo, reduzindo a latência, aumentando a descentralização e simplificando a validação de transações

- Não existem limites arbitrários de blocos ou de tempo para limitar a quantidade de transações que podem ser processadas

- A rede confirmará todas as transações que as condições da rede permitir

### Contas

- Uma conta é a chave pública de um par de chaves público-privada

- Essa chave pública é o endereço da conta, ao qual se detinam as transações

- Um usuário pode ter várias contas

- Cada conta tem uma única chave pública (endereço)

- Além da chave privada que assina os blocos gerados pela conta, existe outra chave privada especial responsável única e exclusivamente para atualizar a versão da estrutura de dados dos blocos na conta.

### Blocos

- Numa criptomoeda baseada em Blockchain, como o Bitcoin, um bloco é um grupo de transações

- Na Nano, um bloco apenas contém detalhes de uma **única transação**

- Há 4 tipos de transações, e blocos, na Nano: Enviar, Receber, Mudar Representante e Epoch

- Para transferir fundos, duas transações são necessárias: uma de Envio e outra de Recebimento

- A terminologia utilizada muda de significado quando num contexto de Bitcoin, por exemplo, e quando num contexto de Nano

- Blocos, transações e transferências têm significados diferentes dos significados atribuídos a essas palavras num contexto de criptomoedas baseadas em Blockchain

- Bloco: codificação digital dos detalhes de uma transação

- Transação: ação de criar e publicar um bloco na rede

- Transferência: é o completar de duas transações, a de Envio e a de Recebimento

#### Porque duas transações por transferência?

Mesmo embora uma transação de Envio confirmada seja irreversível, para que o destinatário possa mover esses fundos novamente, é necessário completar a transferência criando uma transação de Recebimento.

- Enviar fundos pode ser feito enquanto o destinatário está offline

- Apenas os donos das contas podem modificar seu próprio saldo e node representante

- Permite ao dono da conta ignorar transações

### Block Lattice

A estrutura entrelaçada surge dos blocos conectados através das cadeias de contas.

Cada conta é uma blockchain, e os blocos trafegam entre essas blockchains se utilizando de campos específicos no protocolo, como o "previous" e o "link".

- Tudo começa com uma conta gênesis

- A conta gênesis começa contendo a quantidade fixa total de unidades de Nano existentes

- O saldo da conta gênesis nunca pode ser aumentado, ele é o valor máximo de Nanos que podem existir

- A soma total de Nanos em todas as contas existentes nunca poderão exceder o saldo original da conta gênesis

### Podando a Ledger

Ledger Pruning é o termo que se refere a ação de remover tudo aquilo que não é essencial para o histórico da rede.

Com exceção dos blocos envolvidos em transações pendentes, o histórico de todas as contas poderia ser reduzido a apenas um bloco por conta, independente de quantas transferências foram feitas.

## Blocos

Termos chaves desta seção:

- Bloco é a **codificação digital** dos detalhes da transação

- Transação é o **ato** de criar e publicar um bloco na rede

- Transferência é o completar de ambos os blocos de Envio e de Recebimento

---

- Todas as transações no protocolo são feitas através de blocos

- O **estado completo** de uma conta, incluindo seu saldo após cada transação, está registrado em **cada um dos blocos**

- O valor de uma transação é interpretado como a diferença de saldo entre blocos consecutivos

### Bloco de Estado

#### Estrutura de Dados de um Bloco

| Chave                          | Formato RPC                             | Serializado | Descrição                                                    |
| ------------------------------ | --------------------------------------- | ----------- | ------------------------------------------------------------ |
| type (tipo)                    | string (texto)                          | -           | "state" (estado)                                             |
| account (conta)                | string (texto)                          | 32 bytes    | Endereço da conta                                            |
| previous (anterior)            | 64 hex-char string (texto hexadecimal)  | 32 bytes    | Bloco anterior registrado na conta; 0 se for um bloco "open" |
| representative (representante) | string (texto)                          | 32 bytes    | Endereço do representante                                    |
| balance (saldo)                | decimal string (texto decimal)          | 16 bytes    | Saldo resultante (em raw)                                    |
| link                           | -                                       | 32 bytes    | Campo de múltiplos propósitos                                |
| signature (assinatura)         | 128 hex-char string (texto hexadecimal) | 64 bytes    | Assinatura ED25519+Blake2b 512-bit                           |
| work (trabalho)                | 16 hex-char string (texto hexadecimal)  | 8 bytes     | Nonce de Proof of Work                                       |

Dependendo da ação que cada transação tem como intenção performar, o campo "link" terá um valor diferente para o comando RPC *block_create*

| Ação              | Formato RPC                            | Descrição                                    |
| ----------------- | -------------------------------------- | -------------------------------------------- |
| Change (mudar)    | string (texto)                         | Deve ser "0"                                 |
| Send (enviar)     | string (texto)                         | Endereço Nano do destinatário                |
| Receive (receber) | 64 hex-char string (texto hexadecimal) | Hash do bloco de envio referente a transação |

Exemplo de bloco Nano:

```
"block": {
  "type": "state",
  "account": "nano_3qgmh14nwztqw4wmcdzy4xpqeejey68chx6nciczwn9abji7ihhum9qtpmdr",
  "previous": "F47B23107E5F34B2CE06F562B5C435DF72A533251CB414C51B2B62A8F63A00E4",
  "representative": "nano_1hza3f7wiiqa7ig3jczyxj5yo86yegcmqk3criaz838j91sxcckpfhbhhra1",
  "balance": "1000000000000000000000",
  "link": "19D3D919475DEED4696B5D13018151D1AF88B2BD3BCFF048B45031C1F36D1858",
  "link_as_account": "nano_18gmu6engqhgtjnppqam181o5nfhj4sdtgyhy36dan3jr9spt84rzwmktafc",
  "signature": "3BFBA64A775550E6D49DF1EB8EEC2136DCD74F090E2ED658FBD9E80F17CB1C9F9F7BDE2B93D95558EC2F277FFF15FD11E6E2162A1714731B743D1E941FA4560A",
  "work": "cab7404f0b5449d0"
}
```

### Saldo da conta

Se o saldo diminui, a transação que causou a diminuição é considerada uma transação de Envio. Se ele aumenta, a transação que causou o aumento é uma de Recebimento.

### Bloco vs. Transação

Em criptomoedas baseadas em Blockchain, um bloco é um **grupo** de transações. **Na Nano, um bloco é uma única transação**.

Ambos os termos são geralmente intercambiáveis.

- "Transação" refere-se a ação

- "Bloco" refere-se a codificação digital da transação

Transações são assinadas com a chave privada pertencente à conta na qual a transação é performada.

### Criando Transações

#### Open

- Para criar uma conta, uma transação "open" deve ser feita primeiro

- A transação "open" é sempre a primeira transação de uma conta

- Pode ser criada no primeiro recebimento de fundos

- Para abrir uma conta, você deve enviar fundos para ela via transação "send" de outra conta

- Ao criar uma conta, um node representante deve ser escolhido

#### Send

- Diminui o saldo do remetente de acordo com o valor a ser enviado

- Só pode ser criada após uma transação "open" ter sido performada

- O campo "previous" contém o hash do bloco anterior na cadeia de blocos da conta

- O campo "link" contém o endereço do destinatário

- Um bloco "send" é imutável após ter sido confirmado pela rede

- Quando pendente, uma transação nunca pode ser desfeita

- Uma vez efetuada a transação "send", a única forma de alguém mover esses fundos novamente é através de uma transação "receive" por parte do destinatário

#### Receive

- Um bloco "receive" é muito parecido com um bloco "send"

- As únicas diferenças são que o saldo está aumentando e que o campo "link" faz referência ao hash do bloco "send" associado

- Para completar uma transação, o destinatário deve criar um bloco "receive" na Blockchain de sua conta

- Uma vez criado e publicado o bloco "receive", o saldo é atualizado e os fundos são finalmente transferidos para sua conta

#### Change Rep

- Você pode mudar seu representante a qualquer momento

- Um bloco "Change Rep" é o meio para mudar o representante de uma conta

- Isso é possível ao mudar o campo "representative"

#### Epoch

Dado que todas as contas na rede da Nano são assíncronas, um meio assíncrono de atualizações é necessário. Blocos Epoch são a saída que encontraram.

- É um tipo especial de bloco

- Só pode ser gerado pela Nano Foundation através de uma chave privada pré-determinada que está sob o poder deles

- Não é capaz de mudar o saldo ou nodes representates

- Pode apenas atualizar a versão das contas para permitir novas estruturas de blocos (novos campos, etc)

- Esses blocos serão aceitos pelos nodes e serão anexados na blockchain de cada uma das contas da rede, delimitando a fronteira entre as versões

- Caso a maioria da rede não atualize para uma nova versão do node que permite um bloco Epoch em particular, o bloco Epoch terá pouco ou nenhum efeito na rede

- Ao atualizar, as contas serão movidas de Epoch X para Epoch X+1

- Nenhuma futura transação poderá ser feita para contas com um Epoch anterior ao seu

## Spam, Trabalho e Priorização

Uma transação spam é definida como um bloco transmitido com as seguintes intenções:

- Saturar a rede

- Reduzir a disponibilidade da rede

- Aumentar o tamanho da ledger

A Nano possui dois mecanismos de defesa contra spam:

- PoW na criação de blocos

- Sistema de Representação Rotativa de Filas baseadas em Saldo

### PoW - Prova de Trabalho

- Usa Blake2b como algorítmo de Hash

- Tem thresholds constantes

O PoW acontece no processo de criação de Blocos, antes de serem enviados para os nodes.

Seu objetivo é garantir que a dificuldade de se fazer um spam contra a rede aumente linearmente de acordo com o número de transações de spam, reduzindo o impacto na rede.

Se dá da seguinte forma:

$$
H( nonce||x ) ≥ threshold
$$

Onde

- **H** = Algorítmo de Hash, que no caso é o Blake2b

- **x** depende do tipo de bloco
  
  - Caso seja um Bloco Open
    
    - **x** = chave pública da conta na forma hexadecimal
  
  - Caso seja qualquer outro tipo de Bloco
    
    - **x** = hash do Bloco ancestral

- **threshold** depende do tipo de bloco
  
  - Caso seja Bloco Receive
    
    - **threshold** = fffffe0000000000
  
  - Caso seja qualquer outro tipo de bloco
    
    - **threshold** = fffffff800000000



O processo do PoW acontece da seguinte maneira:

1. Gera um **nonce** (exemplo: 1234)
   
   - Número aleatório/pseudo-aleatório que serve para ser usado apenas uma vez (nonce = number used once)

2. Concatena com **x** (exemplo: h45hd0ul7im0bl0c0), resultando em "1234h45hd0ul7im0bl0c0" 

3. Roda o Blake2b usando "1234h45hd0ul7im0bl0c0" como input

4. Verifica se o output do Blake2d é maior ou igual ao threshold
   
   - Caso seja, é uma Prova de Trabalho válida e é usada para preencher o campo "work" do JSON do Bloco enviado aos nodes via RPC
   
   - Caso não seja, o processo é repetido até que se encontre um PoW válido

### Sistema de Representação Rotativa de Filas baseadas em Saldo

- Há 62 filas (buckets, na terminologia original)

- Filas são conjuntos de contas definidos pelo saldo destas
  
  - Fila 1: contas com saldo entre X e Y
  
  - Fila 2: contas com saldo entre Y e W
  
  - Fila 3: contas com saldo entre W e Z
  
  - E assim por diante...

- Nodes Representantes votam em uma transação de cada fila antes de votar numa segunda transação de uma fila
  
  - Exemplo:
    
    1. Votam numa transação da Fila 1
    
    2. Votam numa transação da Fila 2
    
    3. Votam numa transação da Fila 3
    
    4. Votam em transações até a Fila 62
    
    5. Voltam pro começo e repetem o processo.

- Dentro de cada fila, a transação com maior prioridade é a das contas usadas menos recentemente. Ou seja, a prioridade vai para as contas que foram usadas há mais tempo

- Dentro de cada conta numa fila, as transações que serão votadas seguem as seguintes regras:
  
  - Transações cujo Bloco ancestral (campo Previous) ainda não foi confirmado (cemented), são rejeitadas e não entram na fila
  
  - Transações cujo Bloco ancestral está numa fila, não entram na fila, ficam no backlog
  
  - Somente são votadas transações cujo Bloco ancestral tenha sido confirmado e que passem nas demais validações

## Detalhes de Rede

| Porta | Tipo | Padrão       | Detalhes              |
| ----- | ---- | ------------ | --------------------- |
| 7075  | TCP  | Habilitada   | Servidor de bootstrap |
| 7076  | TCP  | Desabilitada | Servidor RPC          |
| 7078  | TCP  | Desabilitada | Servidor WebSocket    |

### Servidor de bootstrap

Transmite a ledger em lotes, do node em questão para os novos nodes

### Servidor RPC

- Recebe e fornece dados, via HTTP (não confundir com HTTPS), no formato JSON, executando ações no node em questão

- Não é recomendado utilizar a wallet atrelada ao Servidor RPC, caso este esteja disponível na rede para as pessoas utilizarem, principalmente quando a configuração `enable_control` estiver como "true" no config-rpc.toml

- Mesmo que esteja como "false", ainda não é recomendado usar a wallet atrelada ao Servidor RPC, pois ele está exposto na internet

### Servidor WebSocket

Permite comunicação com o RPC via protocolo websocket

### Telemetria

A versão 21 da Nano adicionou telemetria nos nodes.

- A telemetria é compartilhada com outros nodes via mensagem `telemetry_ack`

- Nodes podem requisitar telemetria de outros nodes via mensagem `telemetry_req`

- A mensagem `telemetry_req` só é processada pelo Node Destinatário, quando respeitado o intervalo de 60 segundos entre as mensagens

- Ultrapassar repetidas vezes o intervalo de 60 segundos pode colocar o Node Remetente numa blacklist de IPs no Node Destinatário

#### Assinatura da Telemetria

A mensagem `telemetry_ack` é assinada via algorítmo ED25519 (criptografia assimétrica) da seguinte maneira:

```
ED25519(key = node id public key, message = "node id || block count || cemented count|| unchecked count || account count || bandwidth capacity || peer count || protocol version || uptime || genesis block hash || major version || minor version || patch version || pre-release version || maker || timestamp since UTC epoch || active difficulty")
```

### Processo de Procura por Pares

O processo é descrito na seção Implementação dos Nodes, mas após o Node conectar com outro Node usando o handshake do protocolo, as mensagens keepalive que eles mandam entre si contém uma seleção aleatória de IPs de outros 8 Nodes

## Consenso ORV - Open Representative Voting

Open Representative Voting significa Votação Aberta por Representantes

- Cada usuário escolhe um Node Representante

- Nodes Representantes votam em cada transação
  
  - Cada Node Representante tem um peso na sua votação
  
  - O peso de seu voto é determinado pela soma dos saldos das contas representadas por ele

- Cada Node (Representante ou não) confirma cada transação
  
  - A confirmação só se dá após o peso dos votos aprovando a transação superam 67% dos pesos de votação online (quorum)

- Cada transação é processada de forma independente e assíncrona

- A finalidade determinística (deterministic finality) é alcançada em menos de 1 segundo

- Por ser baseada em Block-Lattice, todo e qualquer fork na Nano afeta apenas a BlockChain da conta que produziu o fork
  
  - Forks podem ser criados de forma intencional ou não intencional
    
    - Um exemplo de não intencional é um programador que criou um bot para transferir Nano, e não se atentou aos detalhes, gerando forks na BlockChain da própria conta
    
    - Um exemplo de intencional é um atacante da rede que está tentando fazer um gasto-duplo (inclusive temos casos de atacantes que tentaram e se deram mal)

- Usuários de Nano podem mudar livremente o Representante de suas Contas
  
  - Portanto, são os usuários que decidem, via algo semelhante a uma Democracia Direta, a distribuição do poder de consenso e a descentralização da rede

- Delegar o peso de voto não significa fazer staking de qualquer fundo
  
  - Usuários de Nano podem gastar quantas Nanos eles quiserem e tiverem disponíveis para tal
  
  - Não há necessidade de "congelar" fundos para delegar peso de voto

### Open Representative Voting VS Proof of Stake

Por usar um sistema de consenso baseado em votações por peso, a Nano é muitas vezes comparada a moedas que usam Proof of Stake, mas esse não é o caso

As diferenças são:

- Nano não tem uma BlockChain monolítica que depende da seleção de um líder para a extender

- Representantes não criam ou produzem blocos compartilhados

- Cada Conta Nano possui sua própria BlockChain, e apenas ela a pode modificar

- Na Nano, um Bloco é apenas uma única transação
  
  - Cada transação é validada individualmente e de forma assíncrona

- Usuários podem delegar seu peso de voto para qualquer um e a qualquer momento

- Na Nano, qualquer um pode ser um Representante

- Na Nano, nenhum fundo é "congelado" ou "travado"

- Representantes não ganham taxas de transação

- Representantes não podem reverter transações que os outros Nodes têm como confirmadas localmente

### Velocidade de Confirmação

- Uma ledger na estrutura Block-Lattice substitui um Acordo em Tempo de Execução (como é no Bitcoin) por um Acordo no Tempo de Design
  
  - A forma que o design está feito, permite que as transações sejam confirmadas sem depender de fatores em tempo de execução

- Um Bloco é uma transação individual que é processada individualmente e de forma assíncrona
  
  - No Bitcoin, um bloco é um conjunto de diversas transações e esses blocos são processados de forma síncrona

- O ORV é leve e minimiza contenções

### Representantes Principais (PRs) e Não-Principais

Há dois tipos de Representantes na Nano:

- Representantes Principais (PR)

- Representantes Não-Principais

Para se tornar um Representante Principal, sua conta Nano deve ter delegado à ela ao menos 0.1% de todo o peso online de votação.

A única diferença operacional entre esses tipos de Representantes é que os votos dos Representantes Principais são retransmitidos por outros nodes que os recebem, ajudando a rede a alcançar o consenso mais rapidamente

Essa decisão de implementação se deu por causa do custo exponencial de banda-larga de permitir que cada Node pudesse enviar seus votos para cada um dos outros Nodes

De qualquer forma, a grande maioria dos Nodes com poder de voto abaixo de 0.1%, já não seriam capazes de contribuir de forma significativa com o consenso da rede
